const express = require('express');
const news = require('./app/news');
const mysql = require('mysql');

const app = express();
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'user',
    password : '1qaz@WSX29',
    database : 'news'
});

app.use('/news', news(connection));

connection.connect(function(err) {
    if (err) {
        console.error('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});