const express = require('express');

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `news_item`', function (error, results) {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }

            res.send(results);
        });
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `news_item` WHERE `id` = ?', req.params.id, function (error, results) {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }

            res.send(results);
        });
    });

    return router;

};

module.exports = createRouter;